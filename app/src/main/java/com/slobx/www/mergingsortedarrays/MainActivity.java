package com.slobx.www.mergingsortedarrays;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int[] firstArray = {1, 3, 6, 9, 15, 45, 46};
    int[] secondArray = {1, 2, 6, 9, 16, 22, 25, 77};
    int[] result;
    StringBuilder sb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sb = new StringBuilder();

        result = merge(firstArray, secondArray);
        for (int i = 0; i < result.length; i++) {
            sb.append(result[i]);
            sb.append(", ");
        }

        makeToast(sb.toString());
    }

    public static int[] merge(int[] a, int[] b) {
        int[] answer = new int[a.length + b.length];
        int i = 0, j = 0, k = 0;

        while (i < a.length && j < b.length)
            answer[k++] = a[i] < b[j] ? a[i++] : b[j++];

        while (i < a.length)
            answer[k++] = a[i++];


        while (j < b.length)
            answer[k++] = b[j++];


        int data[] = removeDuplicates(answer);
        return data;
    }

    public static int[] removeDuplicates(int[] a) {
        int end = a.length;

        for (int i = 0; i < end; i++) {
            for (int j = i + 1; j < end; j++) {
                if (a[i] == a[j]) {
                    int shiftLeft = j;
                    for (int k = j + 1; k < end; k++, shiftLeft++) {
                        a[shiftLeft] = a[k];
                    }
                    end--;
                    j--;
                }
            }
        }

        int[] finalList = new int[end];
        for (int i = 0; i < end; i++) {
            finalList[i] = a[i];
        }
        return finalList;
    }


    private void makeToast(String toastText) {
        Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_LONG).show();
    }
}
